import Vue from 'vue';
import Router from 'vue-router';
import home from '../components/pages/home';
import aboutUs from '../components/pages/aboutUs';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: '/fk_luch/',
  routes: [{
    path: '/',
    name: 'home',
    component: home,
  }, {
    path: '/aboutUs',
    name: 'aboutUs',
    component: aboutUs,
  }, {
    path: '/*',
    redirect: '/',
  }],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => {
  next();
});

export default router;
